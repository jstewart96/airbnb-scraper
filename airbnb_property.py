class AirbnbProperty(object):

    def __init__(self, url):
        self.url = url
        self._amenities = []

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, type):
        self._type = type

    @property
    def bathrooms(self):
        return self._bathrooms

    @bathrooms.setter
    def bathrooms(self, bathrooms):
        self._bathrooms = bathrooms

    @property
    def bedrooms(self):
        return self._bedrooms

    @bedrooms.setter
    def bedrooms(self, bedrooms):
        self._bedrooms = bedrooms

    @property
    def amenities(self):
        return self._amenities

    @amenities.setter
    def amenities(self, amenities):
        self._amenities = amenities

    def get_amenities_formatted_string(self):
        output = ''

        for amenity_section in self.amenities:
            output = output + '  {}:\n'.format(amenity_section['section'])
            for amenity in amenity_section['amenities']:
                output = output + '  - {}\n'.format(amenity['title'])
                if 'subtitle' in amenity:
                    output = output + '  -- {}\n'.format(amenity['subtitle'])

        return output

    def __str__(self):

        return """Property:
Title: {},
Type: {},
Bedrooms: {}, Bathrooms: {},
Amenities:
{}
        """.format(self.title, self.type, self.bedrooms, self.bathrooms, self.get_amenities_formatted_string())