# AirBnb Scraper

> Written in Python 3.6.5
> 
> Utilising packages `requests` and `selenium`

## Pre-requisites

* `pipenv`
	* `brew install pipenv`
* Firefox
	* `brew cask install Firefox`
* `geckodriver`
	* `brew install geckodriver`

## Usage

* Install deps:

`pipenv install`

* The task has been coded in `./airbnb_scraper.py`. Execute that file to run the program for the requested URLs:

```
> pipenv shell
>> python airbnb_scraper.py
```	

* Output *should* look like:

```
https://www.airbnb.co.uk/rooms/19292873?s=51 is NOT a valid AirBnB property

Property:
Title: York Place: Luxurious apartment For Two adults.,
Type: Entire Flat,
Bedrooms: 1, Bathrooms: 1,
Amenities:
  Basic:
  - Wireless Internet
  -- Continuous access in the listing
  - TV
  - Laptop friendly workspace
  -- A table or desk with space for a laptop and a chair that’s comfortable to work in
  - Iron
  - Essentials
  -- Towels, bed sheets, soap, and toilet paper
  - Washer
  -- In the building, free or for a fee
  - Heating
  -- Central heating or a heater in the listing
  - Hot water
  Facilities:
  - Paid parking on premises
  Dining:
  - Kitchen
  -- Space where guests can cook their own meals
  Guest access:
  - Lockbox
  - Private entrance
  -- Separate street or building entrance
  Bed and bath:
  - Hair dryer
  - Shampoo
  - Hangers
  - Bed linens
  Safety features:
  - Carbon monoxide detector
  - Smoke detector




Property:
Title: York Place: Luxurious apartment For Two adults.,
Type: Entire Flat,
Bedrooms: 1, Bathrooms: 1,
Amenities:
  Basic:
  - Wireless Internet
  -- Continuous access in the listing
  - TV
  - Laptop friendly workspace
  -- A table or desk with space for a laptop and a chair that’s comfortable to work in
  - Iron
  - Essentials
  -- Towels, bed sheets, soap, and toilet paper
  - Washer
  -- In the building, free or for a fee
  - Heating
  -- Central heating or a heater in the listing
  - Hot water
  Facilities:
  - Paid parking on premises
  Dining:
  - Kitchen
  -- Space where guests can cook their own meals
  Guest access:
  - Lockbox
  - Private entrance
  -- Separate street or building entrance
  Bed and bath:
  - Hair dryer
  - Shampoo
  - Hangers
  - Bed linens
  Safety features:
  - Carbon monoxide detector
  - Smoke detector



```

* Tests are in `./test_airbnb_scraper.py`

```
> pipenv shell
>> python test_airbnb_scraper.py
```

## Un-implemented functionality

* Logging
	* Would look to include some informational logging for normal execution, and a debug-level that can be optionally set
* CLI
	* Provide a CLI that can take in an array of property URLs as positional args to invoke this code
