#TODO Mock out the logger?

import unittest
from unittest import mock
from unittest.mock import Mock, create_autospec, patch, call

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.webelement import FirefoxWebElement
from selenium.webdriver import Firefox

from requests import Response, ConnectionError, RequestException, HTTPError

import airbnb_scraper as UNIT


def check_find_single_xpath_throws_no_such_element_exception(testcase, func):
    mock_webdriver = mock.create_autospec(Firefox)
    mock_webdriver.find_element_by_xpath = Mock(side_effect=NoSuchElementException)
    testcase.assertRaises(NoSuchElementException, func, mock_webdriver)

def check_find_multiple_xpath_throws_no_such_element_exception(testcase, func):
    mock_webdriver = mock.create_autospec(Firefox)
    mock_webdriver.find_elements_by_xpath = Mock(side_effect=NoSuchElementException)
    testcase.assertRaises(NoSuchElementException, func, mock_webdriver)

def check_find_single_xpath_called_with_xpath(testcase, func, xpath):
    mock_webdriver = mock.create_autospec(Firefox)
    func(mock_webdriver)
    mock_webdriver.find_element_by_xpath.assert_called_with(xpath)

def check_find_multiple_xpath_called_with_xpath(testcase, func, xpath):
    mock_webdriver = mock.create_autospec(Firefox)
    func(mock_webdriver)
    mock_webdriver.find_elements_by_xpath.assert_called_with(xpath)

def amenity_title_exception(*args, **kwargs):
    if './parent::h2' or './../preceding-sibling::div[text()]' in args:
        raise NoSuchElementException

def amenity_subtitle_exception(*args, **kwargs):
    if './parent::h2' in args:
        raise NoSuchElementException


class ValidatePropertyUrls(unittest.TestCase):

    @patch('airbnb_scraper.requests', create_autospec=True)
    def test_all_properties_valid(self, mock_requests):
        
        mock_valid_response = mock.create_autospec(Response)
        mock_valid_response.status_code = 200

        mock_requests.head.return_value = mock_valid_response
        
        urls = [
            'https://property1.com',
            'https://property2.com',
            'https://property3.com'
            ]

        valid_urls = UNIT.get_valid_property_urls(urls)

        self.assertEqual(3, len(valid_urls))

        mock_requests.head.assert_has_calls(
            [
                call('https://property1.com'),
                call('https://property2.com'),
                call('https://property3.com')
            ]
        )

    @patch('airbnb_scraper.requests', create_autospec=True)
    def test_one_property_invalid(self, mock_requests):
        
        mock_valid_response = mock.create_autospec(Response)
        mock_valid_response.status_code = 200

        mock_not_found_response = mock.create_autospec(Response)
        mock_not_found_response.status_code = 404

        mock_requests.head.side_effect = [mock_valid_response, mock_not_found_response, mock_valid_response]

        urls = [
            'https://property1.com',
            'https://property2.com',
            'https://property3.com'
            ]

        valid_urls = UNIT.get_valid_property_urls(urls)

        self.assertEqual(2, len(valid_urls))

        mock_requests.head.assert_has_calls(
            [
                call('https://property1.com'),
                call('https://property2.com'),
                call('https://property3.com')
            ]
        )

    @patch('airbnb_scraper.requests', create_autospec=True)
    def test_raises_exception_if_cannot_obtain_result(self, mock_requests):

        mock_requests.head.side_effect = [ConnectionError, HTTPError]

        self.assertRaises(RequestException, UNIT.get_valid_property_urls, ['https://property1.com'])
        self.assertRaises(RequestException, UNIT.get_valid_property_urls, ['https://property1.com'])
        self.assertEqual(2, mock_requests.head.call_count)

        
class GetTitleTest(unittest.TestCase):

    def test_get_title_element_found(self):
        check_find_single_xpath_called_with_xpath(self, UNIT.get_title_element, '//div[@id="summary"]//div[@itemprop="name"]//h1')


    def test_get_title_element_not_found(self):
        check_find_single_xpath_throws_no_such_element_exception(self, UNIT.get_title_element)

    def test_get_title_text_found(self):
        
        text = 'Stunning Playas Villa in the Hills'
        
        mock_element = mock.create_autospec(FirefoxWebElement)
        mock_element.text = text

        output_text = UNIT.get_title_text(mock_element)
        self.assertEqual(text, output_text)

    def test_get_title_text_empty(self):
        
        mock_element = mock.create_autospec(FirefoxWebElement)
        mock_element.text = ''

        output_text = UNIT.get_title_text(mock_element)
        self.assertEqual('', output_text)

class GetPropertyTypeTest(unittest.TestCase):

    def test_get_property_type_element_found(self):
        check_find_single_xpath_called_with_xpath(self, UNIT.get_property_type_element, '//div[@id="summary"]//span[contains(@style, "color")]')

    def test_get_property_type_element_not_found(self):
        check_find_single_xpath_throws_no_such_element_exception(self, UNIT.get_property_type_element)

    def test_get_property_type_text_found(self):
        
        text = 'ENTIRE FLAT'
        
        mock_element = mock.create_autospec(FirefoxWebElement)
        mock_element.text = text

        output_text = UNIT.get_property_type_text(mock_element)
        self.assertEqual(text.title(), output_text)

    def test_get_property_type_text_empty(self):
        
        mock_element = mock.create_autospec(FirefoxWebElement)
        mock_element.text = ''

        output_text = UNIT.get_property_type_text(mock_element)
        self.assertEqual('', output_text)

class GetBathroomsTest(unittest.TestCase):
    
    def test_get_bathrooms_element_found(self):
        check_find_single_xpath_called_with_xpath(self, UNIT.get_bathroom_element, '//div[@id="summary"]//span[contains(text(), "bath")]')
    
    def test_get_bathrooms_element_not_found(self):
        check_find_single_xpath_throws_no_such_element_exception(self, UNIT.get_bathroom_element)

    def test_get_bathrooms_int(self):
        bathrooms_text = '2 Bathrooms'

        mock_element = mock.create_autospec(FirefoxWebElement)
        mock_element.text = bathrooms_text

        bathrooms = UNIT.get_number_of_bathrooms(mock_element)

        self.assertEqual(2, bathrooms)

    def test_get_bathrooms_double(self):
        bathrooms_text = '1.5 Bathrooms'

        mock_element = mock.create_autospec(FirefoxWebElement)
        mock_element.text = bathrooms_text

        bathrooms = UNIT.get_number_of_bathrooms(mock_element)

        self.assertEqual(1.5, bathrooms)

    def test_get_bathrooms_no_match(self):
        bathrooms_text = 'NAE Bathrooms'

        mock_element = mock.create_autospec(FirefoxWebElement)
        mock_element.text = bathrooms_text

        bathrooms = UNIT.get_number_of_bathrooms(mock_element)

        self.assertEqual(0, bathrooms)

class GetBedroomsTest(unittest.TestCase):

    def test_get_bedrooms_elements_found(self):
        check_find_multiple_xpath_called_with_xpath(self, UNIT.get_bedroom_elements, '//div[@id="summary"]//span[contains(text(), "bed")]')
        
    
    def test_get_bedrooms_elements_not_found(self):
        mock_webdriver = mock.create_autospec(Firefox)
        mock_webdriver.find_elements_by_xpath = Mock(side_effect=NoSuchElementException)
        self.assertRaises(NoSuchElementException, UNIT.get_bedroom_elements, mock_webdriver)

    def test_get_number_of_bedrooms_bedroom_element_takes_precendence(self):
        mock_bedrooms_element = mock.create_autospec(FirefoxWebElement)
        mock_bedrooms_element.text = '2 BeDrOoms'

        mock_beds_element = mock.create_autospec(FirefoxWebElement)
        mock_beds_element.text = '3 Beds'

        output_bedrooms = UNIT.get_number_of_bedrooms([mock_bedrooms_element, mock_beds_element])

        self.assertEqual(2, output_bedrooms)

    def test_get_number_of_bedrooms_beds_element_only_present(self):

        mock_beds_element = mock.create_autospec(FirefoxWebElement)
        mock_beds_element.text = '1 Bed'

        output_bedrooms = UNIT.get_number_of_bedrooms([mock_beds_element])

        self.assertEqual(1, output_bedrooms)
        

class GetAmenitiesTest(unittest.TestCase):

    def test_get_amenities_elements_found(self):
        mock_webdriver = mock.create_autospec(Firefox)

        mock_webdriver.find_elements_by_xpath.return_value = mock.create_autospec(FirefoxWebElement)
        mock_webdriver.find_element_by_xpath.return_value = mock.create_autospec(FirefoxWebElement)

        UNIT.get_amenities_elements(mock_webdriver)


    def test_get_amenities_elements_not_found(self):
        mock_webdriver = mock.create_autospec(Firefox)
        mock_webdriver.find_element_by_xpath = Mock(side_effect=NoSuchElementException)
        self.assertRaises(NoSuchElementException, UNIT.get_amenities_elements, mock_webdriver)

    def test_get_amenities_array_valid(self):

        mock_section_element = mock.create_autospec(FirefoxWebElement)
        mock_section_element.text = 'Basics'

        mock_amenity_one_title = mock.create_autospec(FirefoxWebElement)
        mock_amenity_one_title.find_element_by_xpath = Mock(side_effect=amenity_title_exception) 
        mock_amenity_one_title.text = 'Kitchen'

        mock_amenity_one_subtitle = mock.create_autospec(FirefoxWebElement)
        mock_amenity_one_subtitle.find_element_by_xpath = Mock(side_effect=amenity_subtitle_exception)
        mock_amenity_one_subtitle.text = 'This property has a kitchen where you can cook'

        mock_not_included_section_element = mock.create_autospec(FirefoxWebElement)
        mock_not_included_section_element.text = 'Not Included'

        amenities_array = UNIT.get_amenities_array([
            mock_section_element, 
            mock_amenity_one_title, 
            mock_amenity_one_subtitle,
            mock_not_included_section_element
        ])

        expected = [
            {
                'section': 'Basics',
                'amenities': [
                    {
                        'title': 'Kitchen',
                        'subtitle': 'This property has a kitchen where you can cook'
                    }
                ]
            }
        ]

        self.assertListEqual(amenities_array, expected)


if __name__ == '__main__':
    unittest.main()