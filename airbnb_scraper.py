import requests
from airbnb_property import AirbnbProperty
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
import json 
import re

def get_title_element(webdriver):
    return webdriver.find_element_by_xpath('//div[@id="summary"]//div[@itemprop="name"]//h1')

def get_title_text(element):
    return element.text

def get_property_type_element(webdriver):
    return webdriver.find_element_by_xpath('//div[@id="summary"]//span[contains(@style, "color")]')

def get_property_type_text(element):
    return element.text.title()
    
def get_bathroom_element(webdriver):
    return webdriver.find_element_by_xpath('//div[@id="summary"]//span[contains(text(), "bath")]')

def get_number_of_bathrooms(element):
    text = element.text
    match = re.search('^[\d\.]+', text)
    if match:
        num = match.group(0)
        return int(num) if '.' not in num else float(num)
    else: 
        return 0

def get_bedroom_elements(webdriver):
    return webdriver.find_elements_by_xpath('//div[@id="summary"]//span[contains(text(), "bed")]')

def get_number_of_bedrooms(elements):
    if len(elements) > 1 :
        
        bedrooms_text = [x for x in elements if 'bedroom' in x.text.lower()][0].text
        match = re.search('^\d+', bedrooms_text)
        return int(match.group(0))

    else:

        beds_text = elements[0].text
        match = re.search('^\d+', beds_text)
        return int(match.group(0))

    

def get_amenities_elements(webdriver):

    amenities_section = webdriver.find_element_by_xpath('//div//h2//span[.="Amenities"]/ancestor::section[1]')
    
    show_all_amenities_button = amenities_section.find_element_by_xpath('//button/span[starts-with(text(),"Show all")]/..')
    webdriver.execute_script('arguments[0].scrollIntoView()', show_all_amenities_button)
    show_all_amenities_button.click()
    
    amenity_modal = webdriver.find_element_by_xpath('//div[@aria-modal and @aria-labelledby="dls-modal__AmenitiesModal"]')
    return amenity_modal.find_elements_by_xpath('.//div[text() and not(descendant::*)]')

def _element_is_amenity_section_title(element):
    try:
        element.find_element_by_xpath('./parent::h2')
        return True
    except NoSuchElementException:
        return False

def _element_is_amenity_subtitle(element):
    try:
        element.find_element_by_xpath('./../preceding-sibling::div[text()]')
        return True
    except NoSuchElementException:
        return False


def get_amenities_array(elements):
    amenities = []

    for element in elements:
        if _element_is_amenity_section_title(element):
            if 'not included' in element.text.lower():
                continue
            amenities.append({"section": element.text, "amenities": []})
        elif _element_is_amenity_subtitle(element):
            amenities[-1]['amenities'][-1].update({'subtitle': element.text})
        else:
            amenities[-1]['amenities'].append({'title': element.text})

    return amenities


def get_property_details(webdriver, property):
    webdriver.get(property.url)

    property.title = get_title_text(get_title_element(webdriver))
    property.type = get_property_type_text(get_property_type_element(webdriver))
    property.bathrooms = get_number_of_bathrooms(get_bathroom_element(webdriver))
    property.bedrooms = get_number_of_bedrooms(get_bedroom_elements(webdriver))
    property.amenities = get_amenities_array(get_amenities_elements(webdriver))

    return property

def get_valid_property_urls(property_urls):
    return list(filter(lambda url: requests.head(url).status_code < 299, property_urls))


if __name__ == '__main__':
    
    properties = []
    
    task_property_urls = [
        'https://www.airbnb.co.uk/rooms/14531512?s=51',
        'https://www.airbnb.co.uk/rooms/19278160?s=51',
        'https://www.airbnb.co.uk/rooms/19292873?s=51'
    ]

    valid_property_urls = get_valid_property_urls(task_property_urls)

    if valid_property_urls:

        options = Options()
        options.set_headless()
        firefox_driver = webdriver.Firefox(options=options)

        try:
            for url in task_property_urls:
                if url not in valid_property_urls:
                    print("{} is NOT a valid AirBnB property\n".format(url))
                else:
                    property = AirbnbProperty(url)
                    properties.append(get_property_details(firefox_driver, property))
        finally:
            firefox_driver.quit()

    else:
        print('NO VALID PROPERTIES PROVIDED')

    for property in properties:
        print(property)
        print('\n')

    

        


        






